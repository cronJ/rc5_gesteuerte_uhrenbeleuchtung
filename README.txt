RC5_gesteuerte_Uhrenbeleuchtung

Author: Alexander Ransmann
Description:

Auswerten eines RC-5 Codes und schalten eines IO Pins f�r
5 Sekunden bei �bereinstimmung.

Messung der Zeit von fallender zu fallender Flanke:

 	3000us => Startbit
 	1800us => logisch 1
 	1200us => logisch 0
 	45ms   => Pause zwischen erstem Befehl und der Wiederholung
 
Es wird nur der Befehlscode gepr�ft, nicht der Adresscode.

- Sony SIRCS Code
- Atmel ATmega644-20P