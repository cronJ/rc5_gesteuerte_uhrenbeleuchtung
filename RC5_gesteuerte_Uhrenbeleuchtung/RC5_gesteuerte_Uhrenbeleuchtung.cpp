/*
 * RC5_gesteuerte_Uhrenbeleuchtung.cpp
 *
 * Created: 03.11.2012 13:21:48
 *  Author: Alexander Ransmann
 *	Description:	Auswerten eines RC-5 Codes und schalten eines IO Pins f�r 5 Sekunden bei �bereinstimmung.
 *					Messung der Zeit von steigender zu steigender Flanke:
 *
 *					3000�s => Startbit
 *					1800�s => logisch 1
 *					1200�s => logisch 0
 *					45ms   => Pause zwischen erstem Befehl und der Wiederholung
 *
 *					Es wird nur der Befehlscode gepr�ft, nicht der Adresscode. Sony SIRCS Code!
 */ 

// Includes
#include <avr/io.h>
#include <avr/interrupt.h>

// Gew�nschter RC-5 Befehl des Senders
#define RC5_CMD 0x5E

// Signal vom TSOP1738 geht an PD6
#define RC5_DDR DDRD
#define RC5_PORT PORTD
#define RC5_IN PD6

// Ausgang f�r Signal an Beleuchtung an PA0
#define LED_DDR DDRA
#define LED_PORT PORTA
#define LED_OUT PA0

// Prototypen
void InitTimer1();
void InitTimer2();
void StartTimer();
void StopTimer();

// Interrupt
ISR(TIMER1_CAPT_vect);
ISR(TIMER0_OVF_vect);

// Variablen
volatile uint16_t ICRValue;

volatile uint8_t RC5_cmd_val = 0x00;

volatile uint8_t CmdBitNumber = 7;
volatile uint8_t StartBit = 0;

volatile uint8_t CmdDone = 0;
volatile uint8_t CmdMatch = 0;

volatile uint32_t TimerValue = 0;

// Programmstart
int main(void)
{
	// Timer initialisieren
	InitTimer1();
	InitTimer2();
	
	LED_DDR = (1 << LED_OUT); 
	
	sei();
	
    while(1)
    {
		if (CmdDone == 1)
		{
			if (RC5_cmd_val == RC5_CMD)
			{
				LED_PORT |= (1 << LED_OUT);
				CmdMatch = 1;
				TimerValue = 0;
				StartTimer();
			}
			
			// R�cksetzen aller Werte
			RC5_cmd_val = 0x00;
			CmdBitNumber = 7;
			StartBit = 0;
			CmdDone = 0;
		}				 			
    }
}

// Berechnung der Signalzeit zwischen zwei Flanken im Interrupt
ISR(TIMER1_CAPT_vect)
{
	// Z�hler wieder auf 0 setzen (TCNT1 befindet sich schon in ICR1)
	TCNT1 = 0;
	
	// Auslesen des Input Capture Registers
	ICRValue = ICR1;

	// Gehe Bits durch
	if (CmdDone == 0 && CmdMatch != 1)
	{
		if (StartBit == 0)
		{
			if (ICRValue > 740 && ICRValue < 760)
			{
				StartBit = 1;
			}
		}
		else
		{
			// Wenn 1800�s lang
			if (ICRValue > 440 && ICRValue < 460)
			{
				RC5_cmd_val |= (1 << --CmdBitNumber);
			}
			else
			{
				CmdBitNumber--;
			}
			if (CmdBitNumber == 0)
			{
				CmdDone = 1;
			}
		}
	}	
}

// Timer nach 5 Sekunden Port aus schalten und Werte f�r Timer1 reseten
ISR(TIMER0_OVF_vect)
{
	TimerValue++;
	
	//LED_PORT |= (1 << LED_OUT);
	
	if (TimerValue == 39063) // 256 * 500ns * 39063 = 5,000064s
	{
		// LED ausschalten
		LED_PORT &= ~(1 << LED_OUT);
		
		// Timer stoppen
		StopTimer();
		
		// R�cksetzen aller Werte
		RC5_cmd_val = 0x00;
		CmdBitNumber = 7;
		StartBit = 0;
		CmdDone = 0;
		CmdMatch = 0;
	}
}

// Initialisiere Timer mit gew�nschter Funktion
void InitTimer1()
{
	// Falling Edge detection | Clk/64= 4�s
	TCCR1B |= ((1 << CS11) | (1 << CS10));
	
	// Input Capture Interupt Enable
	TIMSK1 |= (1 << ICIE1);
	
	// Eingang f�r Signal konfigurieren
	RC5_DDR &= ~(1 << RC5_IN);
}

// Timer um IO Pin 5 Sekunden lang zu schalten
void InitTimer2()
{	
	// Overflow Interrupt Enable
	TIMSK0 |= (1 << TOIE0);
}

void StartTimer()
{
	// Clk/8 => 2MHz = 500ns
	TCCR0B |= (1 << CS01);
}

void StopTimer()
{
	// Timer deaktivieren
	TCCR0B &= ~(1 << CS01);
}
